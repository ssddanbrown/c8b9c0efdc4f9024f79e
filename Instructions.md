## How to use

1. Follow the linux documentation found [here](https://code.visualstudio.com/Docs/setup) to install Visual Studio Code.
2. Copy the contents of code.desktop to ~/.local/share/applications/code.desktop
3. Change `/home/dan/Apps/vsc` in the code.desktop file to the location you extracted the application to.


This how only been tested by myself on ElementaryOS Freya.
